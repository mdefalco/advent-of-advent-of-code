print('Part', 1, sum(map(int,open('input.txt').readlines())))

s = 0
found = set([0])
from itertools import cycle
for n in cycle(map(int,open('input.txt').readlines())):
    s += n
    if s in found:
        break
    found.add(s)

print('Part', 2, s)
