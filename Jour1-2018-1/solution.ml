let rec lit_tout f = 
    try
        let n = int_of_string (String.trim (input_line f)) in
        n :: lit_tout f
    with End_of_file -> close_in f; []
(* Ici il y a une subtilité, si on écrit
   int_of_string (String.trim (input_line f)) :: lit_tout f
   en fait, comme OCaml évalue de droite à gauche, l'appel récursif
   va s'évaluer en boucle sans jamais ne rien lire ! *)

let input = lit_tout (open_in "input.txt")

(* Gentille fonction récursive *)
let rec part1 l =
    match l with
    | [] -> 0
    | a::q -> a + part1 q

(* Pour frimer un peu... *)
let part1 = List.fold_left (+) 0

(* Ce type flow permet de gérer une liste infinie en revenant
   au point de départ. La fonction next est l'équivalent d'un List.hd
   sauf qu'ici, le flow n'est jamais vide, donc on va toujours renvoyer 
   une valeur *)
type 'a flow = {
    data : 'a list;
    mutable current : 'a list
}

let input_flow = { data = input; current = [] }

let rec next f = match f.current with
    | [] -> f.current <- f.data; next f
    | t::q -> f.current <- q; t

(* Notons qu'on aurait pu faire un int array et le faire avec
   des modulo comme dans le code en C *)

(* Version avec grosse table *)
(* Attention qu'il faut être un peu malin avec les négatifs. On crée
   donc un tableau de 2N cases et on va considérer N+S pour l'indice de S.
   en faisant cela, on a 0 <= N+S < 2N <=> -N <= S < N comme plage valide.
   La valeur de N est ici la constante suivante : *)
let bound = 1000000
let rec part2_aux f found acc =
    let n = next f in
    let acc' = n + acc in
    if found.(bound + acc')
    then acc'
    else begin
        found.(bound + acc') <- true;
        part2_aux f found acc'
    end
let part2 f = part2_aux f (Array.make (2 * bound) false) 0

(* Version avec dictionnaire *)
let rec part2_aux f found acc =
    let n = next f in
    let acc' = n + acc in
    if Hashtbl.mem found acc'
    then acc'
    else begin
        Hashtbl.add found acc' ();
        part2_aux f found acc'
    end
let part2 f = part2_aux f (Hashtbl.create 100) 0

let _ =
    Printf.printf "Part1 %d\n" (part1 input);
    Printf.printf "Part2 %d\n" (part2 input_flow)
