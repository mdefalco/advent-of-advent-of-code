#include<stdio.h>
#include<stdbool.h>
#include<string.h>
#include<assert.h>

#define N 1000000

int main(void)
{
    FILE *fp = fopen("input.txt", "r");
    int input[N];
    int input_size = 0;
    // voir le code OCaml pour l'explication
    bool table[2*N];
    // on initialise la table à false avec memset
    memset(table, 0, sizeof(bool)*2*N);

    while(true) {
        int x;
        if(fscanf(fp, "%d\n", &x) == EOF)
            break;
        // on vérifie qu'il n'y a pas de problèmes
        assert(input_size < N);
        input[input_size] = x;
        input_size = input_size+1;
    }

    int somme = 0;
    for(int i = 0; i < input_size; i++)
        somme += input[i];
    printf("Part1 %d\n", somme);

    somme = 0;
    int i = 0;
    table[N] = true;
    while(true) {
        somme += input[i];
        if (table[N+somme])
            break;
        table[N+somme] = true;
        i = (i+1) % input_size;
    }

    printf("Part2 %d\n", somme);
}
