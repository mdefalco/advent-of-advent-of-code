
let initial_chain =
    let f = open_in "input.txt" in
    let s = input_line f in
    List.of_seq (String.to_seq s)

let test_chain = List.of_seq (String.to_seq "dabAcCaCBAcCcaDA")

let react a b =
    a <> b && Char.lowercase_ascii a = Char.lowercase_ascii b

let reduce s =
    let rec reduce_aux pre suff =
        match pre,suff with
        | a::pre', b::suff' when react a b -> reduce_aux pre' suff'
        | _, b::suff' -> reduce_aux (b::pre) suff'
        | _, [] -> List.rev pre
    in reduce_aux [] s

let letters s =
    (* du O(n^2) ici suffit *)
    let rec letters_aux s found =
        match s with
        | [] -> found
        | c::s' -> let cl = Char.lowercase_ascii c in
            letters_aux s' 
                (if List.mem cl found then found else cl::found)
    in letters_aux s []

let reduce_rem s c =
    reduce (List.filter (fun x -> x <> c && x <> Char.uppercase_ascii c) s)

let best_reduce s =
    let l = letters s in
    let red = List.map List.length (List.map (reduce_rem s) l) in
    List.fold_left min (List.length s) red

let _ =
    Printf.printf "Part 1: %d\n" (List.length (reduce initial_chain));
    Printf.printf "Part 2: %d\n" (best_reduce initial_chain)
