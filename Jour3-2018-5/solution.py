inp = open('input.txt').read().strip()

def react(a, b):
    return a.lower() == b.lower() and a != b

def reduce(s):
    p = [s.pop()] # une pile
    while s != []:
        x = s.pop()
        if p == [] or not react(p[-1], x):
            p.append(x)
        else:
            p.pop()
    return p

def best(s):
    found = set()
    m = len(s)
    for c in s:
        c = c.lower()
        if c not in found:
            found.add(c)
            m = min(m, len(reduce(
                list(s.replace(c,'').replace(c.upper(),'')))))
    return m

print('Part 1', len(reduce(list(inp))))
print('Part 2', best(inp))
