#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

bool react(char a, char b)
{
    int m = a - b;
    if(m < 0)
        m = -m;
    return m == 'a' - 'A';
}

// Réduction en place
void reduce(char *s)
{
    int n = strlen(s);
    int fin_prefixe = -1;

    for(int debut_suffixe = 0; debut_suffixe < n; debut_suffixe++)
    {
        if(fin_prefixe >= 0 && react(s[fin_prefixe], s[debut_suffixe]))
        {
            fin_prefixe--;
        }
        else
        {
            fin_prefixe++;
            s[fin_prefixe] = s[debut_suffixe];
        }

    }

    s[fin_prefixe+1] = '\0';
}

void reduce_ignore(char *s, char c)
{
    int n = strlen(s);
    int fin_prefixe = -1;

    for(int debut_suffixe = 0; debut_suffixe < n; debut_suffixe++)
    {
        if(fin_prefixe >= 0 && react(s[fin_prefixe], s[debut_suffixe]))
        {
            fin_prefixe--;
        }
        else if (s[debut_suffixe] != c && s[debut_suffixe] != c + 'a' - 'A')
        {
            fin_prefixe++;
            s[fin_prefixe] = s[debut_suffixe];
        }

    }

    s[fin_prefixe+1] = '\0';
}

int main(void)
{
    FILE *fp = fopen("input.txt", "r");
    fseek(fp, 0, SEEK_END);
    int size = ftell(fp);
    
    char *input = malloc(sizeof(char) * (size+1));
    fseek(fp, 0, SEEK_SET);
    fgets(input, size, fp); // avec size on évite le \n final

    char *copy = strdup(input);
    reduce(copy);
    printf("Part 1 : %ld\n", strlen(copy));
    free(copy);

    unsigned long best = strlen(input);
    for(char c = 'A'; c <= 'Z'; c++)
    {
        char *copy = strdup(input);
        reduce_ignore(copy, c);
        unsigned long m = strlen(copy);
        if (m < best) best = m;
        free(copy);
    }
    printf("Part 2 : %ld\n", best);
}
