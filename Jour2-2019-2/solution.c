#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int execute(int *p, int length, int pc)
{
    int op = p[pc];
    if(op == 1 || op == 2)
    {
        int v1 = p[p[pc+1]];
        int v2 = p[p[pc+2]];
        int v;
        if (op == 1) v = v1+v2;
        else v = v1*v2;
        p[p[pc+3]] = v;
        return execute(p, length, pc+4);
    }
    return p[0];
}

int run_program(int *p, int length, int noun, int verb)
{
    int *pnew = malloc(sizeof(int)*length);
    memcpy(pnew, p, sizeof(int)*length);
    pnew[1] = noun;
    pnew[2] = verb;
    return execute(pnew, length, 0);
}

int search(int *p, int length, int target)
{
    for(int noun = 0; noun < 100; noun++)
        for(int verb = 0; verb < 100; verb++)
            if (run_program(p, length, noun, verb) == target)
                return noun * 100 + verb;
    return -1;
}

int main(void)
{
    // c'est moche, mais ça marche :)
    int program[] = {
#include "input.txt"
    };
    int length = sizeof(program)/sizeof(int);
    
    printf("Part 1: %d\n", run_program(program, length, 12, 2));
    printf("Part 2: %d\n", search(program, length, 19690720));
}
