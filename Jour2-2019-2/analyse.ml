(* On va essayer ici de désassembler et de comprendre le programme *)
(* Pour cela, on remplace le calcul par des entiers en un calcul d'expression *)

(* L'expression finale est alors 
   (1+(V+(797869+(76800*(N*3))))) = 230400 * Noun + 797870 + Verb
pour ce programme *)

let program = [|1;0;0;3;1;1;2;3;1;3;4;3;1;5;0;3;2;1;9;19;1;5;19;23;1;6;23;27;1;27;10;31;1;31;5;35;2;10;35;39;1;9;39;43;1;43;5;47;1;47;6;51;2;51;6;55;1;13;55;59;2;6;59;63;1;63;5;67;2;10;67;71;1;9;71;75;1;75;13;79;1;10;79;83;2;83;13;87;1;87;6;91;1;5;91;95;2;95;9;99;1;5;99;103;1;103;6;107;2;107;13;111;1;111;10;115;2;10;115;119;1;9;119;123;1;123;9;127;1;13;127;131;2;10;131;135;1;135;5;139;1;2;139;143;1;143;5;0;99;2;0;14;0|]

type expr = Noun | Verb | Const of int | Add of expr * expr | Mult of expr * expr

let rec eval e n v =
    match e with
    | Noun -> n
    | Verb -> v
    | Const x -> x
    | Add(e1, e2) -> eval e1 n v + eval e2 n v
    | Mult(e1, e2) -> eval e1 n v * eval e2 n v

let rec pprint e =
    match e with
    | Noun -> "N"
    | Verb -> "V"
    | Const x -> string_of_int x
    | Add(e1, e2) -> "(" ^ pprint e1 ^ "+" ^ pprint e2 ^ ")"
    | Mult(e1, e2) -> "(" ^ pprint e1 ^ "*" ^ pprint e2 ^ ")"

(* Simplification d'expression un peu clunky *)
let rec taille e =
    match e with
    | Add(e1, e2) | Mult(e1, e2) -> taille e1 + taille e2 + 1
    | _ -> 1

let rec simpl e =
    match e with
    | Add(e1, e2) when taille e1 > taille e2 -> simpl (Add(e2, e1))
    | Mult(e1, e2) when taille e1 > taille e2 -> simpl (Mult(e2, e1))
    | Add(Const x, Add(Const y, e)) -> simpl(Add(Const (x+y), e))
    | Mult(Const x, Mult(Const y, e)) -> simpl(Mult(Const (x*y), e))
    | Add(Const x, Const y) -> Const (x+y)
    | Mult(Const x, Const y) -> Const (x*y)
    | Mult(Add(e1,e2),e3) -> simpl(Add(Mult(e1,e3),Mult(e2,e3)))
    | Mult(e3,Add(e1,e2)) -> simpl(Add(Mult(e3,e1),Mult(e3,e2)))
    | Add(e1, e2) -> let e' = Add(simpl e1, simpl e2) in
                     if e' = e then e' else simpl e'
    | Mult(e1, e2) -> let e' = Mult(simpl e1, simpl e2) in
                     if e' = e then e' else simpl e'
    | _ -> e

let instr program i n v = eval program.(i) n v
let read_mem program i n v = program.(instr program i n v)
let write_mem program i n v value = program.(instr program i n v) <- value

(* Exécution directe, on pourrait regrouper les deux filtrages *)
let rec execute p pc n v =
    match instr p pc n v with
    | 1 -> 
        write_mem p (pc+3) n v (Add(read_mem p (pc+1) n v, read_mem p (pc+2) n v)); 
        execute p (pc+4) n v
    | 2 ->
        write_mem p (pc+3) n v (Mult(read_mem p (pc+1) n v, read_mem p (pc+2) n v)); 
        execute p (pc+4) n v
    | 99 -> p.(0)
    | _ -> failwith "invalid instruction"

let run_program noun verb =
    let p = Array.map (fun i -> Const i) program in
    p.(1) <- Noun;
    p.(2) <- Verb;
    pprint (simpl (execute p 0 noun verb))


