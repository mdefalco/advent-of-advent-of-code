
(* Plus simple ici de ne pas lire le fichier mais juste de créer la valeur *)
let program = [|1;0;0;3;1;1;2;3;1;3;4;3;1;5;0;3;2;1;9;19;1;5;19;23;1;6;23;27;1;27;10;31;1;31;5;35;2;10;35;39;1;9;39;43;1;43;5;47;1;47;6;51;2;51;6;55;1;13;55;59;2;6;59;63;1;63;5;67;2;10;67;71;1;9;71;75;1;75;13;79;1;10;79;83;2;83;13;87;1;87;6;91;1;5;91;95;2;95;9;99;1;5;99;103;1;103;6;107;2;107;13;111;1;111;10;115;2;10;115;119;1;9;119;123;1;123;9;127;1;13;127;131;2;10;131;135;1;135;5;139;1;2;139;143;1;143;5;0;99;2;0;14;0|]

(* Des fonctions pour rendre la manipulation plus simple avec la double
   indirection *)
let instr program i = program.(i)
let read_mem program i = program.(instr program i)
let write_mem program i v = program.(instr program i) <- v

(* Exécution directe, on pourrait regrouper les deux filtrages *)
let rec execute p pc =
    match instr p pc with
    | 1 -> write_mem p (pc+3) (read_mem p (pc+1) + read_mem p (pc+2)); execute p (pc+4)
    | 2 -> write_mem p (pc+3) (read_mem p (pc+1) * read_mem p (pc+2)); execute p (pc+4)
    | 99 -> p.(0)
    | _ -> failwith "invalid instruction"

let run_program noun verb =
    let p = Array.copy program in
    p.(1) <- noun;
    p.(2) <- verb;
    execute p 0

(* Ici, on se sert d'une exception pour interrompre le flot de controle *)
(* Comme il n'y a qu'une solution, on pourrait se contenter de la double boucle *)
exception Found of int * int
let search target =
    try
        for noun = 0 to 99 do
            for verb = 0 to 99 do
                if run_program noun verb = target
                then raise (Found (noun, verb))
            done
        done;
        raise Not_found
    with Found (noun, verb) -> noun * 100 + verb

let _ = 
    Printf.printf "Part 1 : %d\n" (run_program 12 2);
    Printf.printf "Part 2 : %d\n" (search 19690720)

