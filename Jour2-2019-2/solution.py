prog = eval('['+open('input.txt').read()+']')

def execute(p,pc):
    op = p[pc]
    if op in [1,2]:
        v1, v2 = p[p[pc+1]], p[p[pc+2]]
        p[p[pc+3]] = v1+v2 if op == 1 else v1*v2
        return execute(p,pc+4)
    elif op == 99:
        return p[0]

def run_program(noun, verb):
    p = prog.copy()
    p[1] = noun
    p[2] = verb
    return execute(p, 0)

from itertools import product
def search(target):
    for noun, verb in product(range(100), repeat=2):
        if run_program(noun, verb) == target:
            return noun * 100 + verb

print('Part',1,run_program(12, 2))
print('Part',2,search(19690720))

