(* Ici, je vous recommande de retraiter votre entrée
   pour en faire une de couples OCaml.
   Avec Vim, on peut faire %s/^/(/ et %s/$/);/ 
    
   Je donne quand même le code pour parser le fichier *)

(* Version sans scanf *)
let points =
    let f = open_in "input.txt" in
    let rec lit () =
        try
            let l = input_line f in
            match String.split_on_char ',' l with
            [x;y] -> 
                (int_of_string x, int_of_string (String.trim y))
                :: lit ()
            | _ -> failwith "fichier invalide"
        with End_of_file -> []
    in Array.of_list (lit ())

(* Avec scanf *)
let points =
    let f = open_in "input.txt" in
    let fc = Scanf.Scanning.from_channel f in
    let rec lit () =
        try
            let x, y = 
                Scanf.bscanf fc "%d, %d\n" 
                (fun x y  -> (x,y))
            in
            (x,y) :: lit ()
        with End_of_file -> []
    in Array.of_list (lit ())

(* les dimensions de la grille de travail *)
let minx = Array.fold_left min (fst points.(0))
    (Array.map fst points)
let miny = Array.fold_left min (snd points.(0))
    (Array.map snd points)
let maxx = Array.fold_left max (fst points.(0))
    (Array.map fst points)
let maxy = Array.fold_left max (snd points.(0))
    (Array.map snd points)

let bordure x y =
       x = minx || x = maxx
    || y = miny || y = maxy

let distance (x,y) (x',y') =
    abs (x-x') + abs (y-y')

let part1 () =
    let n = Array.length points in
    let finite = Array.make n true in
    let count = Array.make n 0 in

    for y = miny to maxy do
        for x = minx to maxy do
            let mini = ref (Some 0) in
            let mind = ref (distance (x,y) points.(0)) in
            for i = 1 to n-1 do
                let d = distance (x,y) points.(i) in
                if d = !mind
                then mini := None;
                if d < !mind
                then begin
                    mind := d;
                    mini := Some i
                end
            done;

            match !mini with
            | None -> ()
            | Some i -> begin
                count.(i) <- 1 + count.(i);
                if bordure x y
                then finite.(i) <- false
            end
        done
    done;

    Array.fold_left max 0
        (Array.mapi
            (fun i c -> if finite.(i) then c else 0)
            count)

let part2 tgt =
    let safe = ref 0 in
    for y = miny to maxy do
        for x = minx to maxy do
            let d =
                Array.fold_left (+) 0
                    (Array.map (distance (x,y)) points)
            in if d < tgt then incr safe
        done
    done;
    !safe

let _ =
    Printf.printf "Part 1 : %d\n" (part1 ());
    Printf.printf "Part 2 : %d\n" (part2 10000)

