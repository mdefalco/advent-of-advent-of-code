# lecture de l'entrée
points = []
points_x = []
points_y = []
for l in open('input.txt'):
    x, y = map(int, l.split(','))
    points_x.append(x)
    points_y.append(y)
    points.append( (x, y) )

# On calcule les dimensions exactes de la grille utile.
# Si un point est plus proche d'un des bords de la grille, il est infini
minx, maxx = min(points_x), max(points_x)
miny, maxy = min(points_y), max(points_y)

def part1():
    finite = [ True ] * len(points)
    count = [ 0 ] * len(points)
    for y in range(miny,maxy+1):
        for x in range(minx,maxx+1):
            mind = None
            mini = []
            for i, (a, b) in enumerate(points):
                d = abs(a-x)+abs(b-y)
                if mind == None or mind > d:
                    mind = d
                    mini = [i]
                elif mind == d:
                    mini.append(i)
            if len(mini) == 1:
                count[mini[0]] += 1
                if x in [minx,maxx] or y in [miny,maxy]:
                    finite[mini[0]] = False

    l = []
    for i, (a,b) in enumerate(points):
        if finite[i]:
            l.append(count[i])

    return max(l)

def part2(tgt):
    safe = 0

    for y in range(miny,maxy+1):
        for x in range(minx,maxx+1):
            mind = None
            mini = []
            sd = 0
            for i, (a, b) in enumerate(points):
                sd += abs(a-x)+abs(b-y)
            if sd < tgt:
                safe += 1

    return safe

print('Part',1,part1())
print('Part',2,part2(10000))
