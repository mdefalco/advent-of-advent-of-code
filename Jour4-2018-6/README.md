Jour 4 - 2018/6
===============

[Problème](https://adventofcode.com/2018/day/6)

Cellules infinies et norme 1
----------------------------

Un point très important ici dans l'usage de la norme 1, est que si on a un
point sur le bord, par exemple ayant de coordonnées
$M = (x_M,y)$ où $x_M$ est la plus grande abscisse des points considérés et que
ce
point est plus proche d'un point $A =(x_0,y_0)$
alors pour tout autre point $B = (x',y')$ on a
$|x_M-x_0| + |y - y_0| < |x_M-x'|+|y-y'|$.

Soit maintenant un point $N = (x_M + k,y)$ avec $k \ge 0$ situé à droite de
$M$, on va avoir
$|x_M +k - x_0| + |y-y_0|
= |x_M-x_0| + |y-y_0| + k
< |x_M - x'| + |y-y'| + k
= |x_M + k - x'| + |y-y'|$.

Autrement dit, $N$ est également plus proche de $A$
que de tout autre point. Ainsi, la cellule de $M$ est infinie.

**Autrement dit, un point $A$ a une cellule de taille infinie si et seulement si il
y a un point du carré englobant qui est plus proche de $A$ que de toute autre
point.**
