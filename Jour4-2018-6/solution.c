#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int count_lines(FILE *fp)
{
    int count = 0;
    while(!feof(fp))
    {
        if(fgetc(fp) == '\n') count++;
    }
    return count;
}

int distance(int x1,int y1,int x2,int y2)
{
    return abs(x1-x2) + abs(y1-y2);
}

int part1(int *points_x, int *points_y, int npoints)
{
    int minx = points_x[0];
    int maxx = points_x[0];
    int miny = points_y[0];
    int maxy = points_y[0];
    for(int i = 1; i < npoints; i++)
    {
        if(points_x[i] < minx) minx = points_x[i];
        if(points_x[i] > maxx) maxx = points_x[i];
        if(points_y[i] < miny) miny = points_y[i];
        if(points_y[i] > maxy) maxy = points_y[i];
    }

    bool *finite = malloc(sizeof(bool) * npoints);
    int *count  = malloc(sizeof(int) * npoints);
    for (int i = 0; i < npoints; i++)
    {
        count[i] = 0;
        finite[i] = true;
    }

    for(int x = minx; x <= maxx; x++)
        for(int y = miny; y <= maxy; y++)
        {
            int mind = maxx+maxy;
            int mini = -1;
            for(int i = 0; i < npoints; i++)
            {
                int d = distance(x,y,points_x[i],points_y[i]);
                if (d == mind)
                {
                    mini = -1;
                }
                else if (d < mind)
                {
                    mini = i;
                    mind = d;
                }
            }

            if (mini >= 0)
            {
                count[mini]++;
                if (x == minx && x == maxx && y == miny && y == maxy)
                    finite[mini] = false;
            }
        }

    int max_finite = -1;
    for (int i = 0; i < npoints; i++)
    {
        if(finite[i])
        {
            if(max_finite == -1 || count[max_finite] < count[i])
                max_finite = i;
        }
    }
    
    return count[max_finite];
}

int part2(int *points_x, int *points_y, int npoints, int target)
{
    int safe = 0;
    int minx = points_x[0];
    int maxx = points_x[0];
    int miny = points_y[0];
    int maxy = points_y[0];
    for(int i = 1; i < npoints; i++)
    {
        if(points_x[i] < minx) minx = points_x[i];
        if(points_x[i] > maxx) maxx = points_x[i];
        if(points_y[i] < miny) miny = points_y[i];
        if(points_y[i] > maxy) maxy = points_y[i];
    }


    for(int x = minx; x <= maxx; x++)
        for(int y = miny; y <= maxy; y++)
        {
            int s = 0;
            for (int i = 0; i < npoints; i++)
                s += distance(x, y, points_x[i], points_y[i]);
            if (s < target)
                safe++;
        }

    return safe;
}

int main(void)
{
    FILE *fp = fopen("input.txt", "r");
    int npoints = count_lines(fp);
    fseek(fp, 0, SEEK_SET);

    int *points_x = malloc(sizeof(int) * npoints);
    int *points_y = malloc(sizeof(int) * npoints);

    for(int i = 0; i < npoints; i++)
    {
        fscanf(fp, "%d, %d\n", &points_x[i], &points_y[i]);
    }
    fclose(fp);

    printf("Part 1: %d\n", part1(points_x, points_y, npoints));
    printf("Part 2: %d\n", part2(points_x, points_y, npoints, 10000));

    free(points_x);
    free(points_y);
}
